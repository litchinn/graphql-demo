package com.yema.graphql.demo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yema.graphql.demo.entity.Device;


/**
 * @Author: wanjianghongfeng@yematech.cn
 * @Description:  Mapper 接口
 * @Date: 2021-07-08
 **/
public interface DeviceDao extends BaseMapper<Device> {

}
