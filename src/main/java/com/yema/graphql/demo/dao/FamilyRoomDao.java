package com.yema.graphql.demo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yema.graphql.demo.entity.FamilyRoom;


/**
 * @Author: wanjianghongfeng@yematech.cn
 * @Description:  Mapper 接口
 * @Date: 2021-07-08
 **/
public interface FamilyRoomDao extends BaseMapper<FamilyRoom> {

}
