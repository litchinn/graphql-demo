package com.yema.graphql.demo.component;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yema.graphql.demo.dao.DeviceDao;
import com.yema.graphql.demo.dao.FamilyDao;
import com.yema.graphql.demo.dao.FamilyRoomDao;
import com.yema.graphql.demo.entity.Device;
import com.yema.graphql.demo.entity.Family;
import com.yema.graphql.demo.entity.FamilyRoom;
import graphql.schema.DataFetcher;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * description: //TODO
 *
 * @author wanjianghongfeng@yematech.cn
 * @version 1.0.0
 * @date 2021/7/8 下午3:25
 */
@Component
public class GraphQLDataFetchers {

    @Resource
    private FamilyDao familyDao;
    @Resource
    private FamilyRoomDao familyRoomDao;
    @Resource
    private DeviceDao deviceDao;

    public DataFetcher getFamilyByIdDataFetcher() {
        return dataFetchingEnvironment -> {
            String familyId = dataFetchingEnvironment.getArgument("id");
            return familyDao.selectById(familyId);
        };
    }

    public DataFetcher getRoomDataFetcher() {
        return dataFetchingEnvironment -> {
            Family family = dataFetchingEnvironment.getSource();
            Long familyId = family.getId();
            QueryWrapper<FamilyRoom> queryWrapper = new QueryWrapper<>();
            queryWrapper.lambda().eq(FamilyRoom::getFamilyId, familyId);
            return familyRoomDao.selectList(queryWrapper);
        };
    }

    public DataFetcher getDeviceDataFetcher() {
        return dataFetchingEnvironment -> {
            FamilyRoom room = dataFetchingEnvironment.getSource();
            Long roomId = room.getId();
            QueryWrapper<Device> queryWrapper = new QueryWrapper<>();
            queryWrapper.lambda().eq(Device::getPositionId, roomId);
            return deviceDao.selectList(queryWrapper);
        };
    }
}
