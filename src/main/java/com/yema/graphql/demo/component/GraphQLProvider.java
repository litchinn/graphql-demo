package com.yema.graphql.demo.component;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URL;

/**
 * description: //TODO
 *
 * @author wanjianghongfeng@yematech.cn
 * @version 1.0.0
 * @date 2021/7/8 下午3:08
 */
@Component
public class GraphQLProvider {
    private GraphQL graphQL;

    @Autowired
    GraphQLDataFetchers graphQLDataFetchers;

    @Bean
    public GraphQL graphQL() {
        return this.graphQL;
    }

    @PostConstruct
    public void init() throws IOException {
        URL url = Resources.getResource("schema.graphqls");
        String sdl = Resources.toString(url, Charsets.UTF_8);
        GraphQLSchema graphQLSchema = buildSchema(sdl);
        this.graphQL = GraphQL.newGraphQL(graphQLSchema).build();
    }

    private GraphQLSchema buildSchema(String sdl) {
        TypeDefinitionRegistry typeRegistry = new SchemaParser().parse(sdl);
        RuntimeWiring runtimeWiring = buildWiring();
        SchemaGenerator schemaGenerator = new SchemaGenerator();
        return schemaGenerator.makeExecutableSchema(typeRegistry, runtimeWiring);
    }

    private RuntimeWiring buildWiring() {
        return RuntimeWiring.newRuntimeWiring()
                .type(TypeRuntimeWiring.newTypeWiring("FamilyInfoQuery")
                    .dataFetcher("familyById", graphQLDataFetchers.getFamilyByIdDataFetcher()))
                .type(TypeRuntimeWiring.newTypeWiring("Family")
                    .dataFetcher("rooms", graphQLDataFetchers.getRoomDataFetcher()))
                .type(TypeRuntimeWiring.newTypeWiring("Room")
                    .dataFetcher("devices", graphQLDataFetchers.getDeviceDataFetcher()))
                .build();
    }
}
