package com.yema.graphql.demo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;

/**
 * @Author: wanjianghongfeng@yematech.cn
 * @Description: entity
 * @Date: 2021-07-08
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("fh_family")
public class Family implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;

    @TableField("name")
    private String name;

    @TableField("organ_id")
    private Long organId;

    @TableField("address")
    private String address;

    @TableField("aqara_position_id")
    private String aqaraPositionId;

    @TableField("open_door_type")
    private String openDoorType;

    @TableField("open_lock_password")
    private String openLockPassword;

    @TableField("open_lock_position")
    private String openLockPosition;

    @TableField("open_lock_image")
    private String openLockImage;

    @TableField("family_status")
    private Integer familyStatus;

    @TableField("night_begin_time")
    private Date nightBeginTime;

    @TableField("night_end_time")
    private Date nightEndTime;

    @TableField("pay_money")
    private Double payMoney;

    @TableField("children_num")
    private Integer childrenNum;

    @TableField("pets_state")
    private String petsState;

    @TableField("last_push_msgtime")
    private Date lastPushMsgtime;

    @TableField("apply_user_id")
    private Long applyUserId;

    @TableField("apply_time")
    private Date applyTime;

    @TableField("apply_content")
    private String applyContent;

    @TableField("approval_status")
    private Integer approvalStatus;

    @TableField("approval_pass_time")
    private Date approvalPassTime;

    @TableField("approval_content")
    private String approvalContent;

    @TableField("create_by")
    private String createBy;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_by")
    private String updateBy;

    @TableField("update_time")
    private Date updateTime;

    @TableField("del_flag")
    private Boolean delFlag;

}