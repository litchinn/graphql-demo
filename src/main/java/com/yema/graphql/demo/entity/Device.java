package com.yema.graphql.demo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;

/**
 * @Author: wanjianghongfeng@yematech.cn
 * @Description: entity
 * @Date: 2021-07-08
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("fh_device")
public class Device implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;

    @TableField("name")
    private String name;

    @TableField("device_code")
    private String deviceCode;

    @TableField("device_type")
    private String deviceType;

    @TableField("platform_type")
    private String platformType;

    @TableField("platform_code")
    private String platformCode;

    @TableField("platform_account")
    private String platformAccount;

    @TableField("platform_password")
    private String platformPassword;

    @TableField("position_id")
    private String positionId;

    @TableField("position_name")
    private String positionName;

    @TableField("family_id")
    private Long familyId;

    @TableField("organ_id")
    private Long organId;

    @TableField("signal_value")
    private Integer signalValue;

    @TableField("electricity")
    private Integer electricity;

    @TableField("last_push_msgtime")
    private Date lastPushMsgtime;

    @TableField("device_status")
    private Integer deviceStatus;

    @TableField("on_line_status")
    private Integer onLineStatus;

    @TableField("up_data_check")
    private Boolean upDataCheck;

    @TableField("up_data_check_time")
    private Date upDataCheckTime;

    @TableField("device_buy_type")
    private Integer deviceBuyType;

    @TableField("create_by")
    private String createBy;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_by")
    private String updateBy;

    @TableField("update_time")
    private Date updateTime;

}