package com.yema.graphql.demo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;

/**
 * @Author: wanjianghongfeng@yematech.cn
 * @Description: entity
 * @Date: 2021-07-08
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("fh_family_room")
public class FamilyRoom implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;

    @TableField("name")
    private String name;

    @TableField("type")
    private Integer type;

    @TableField("family_id")
    private Long familyId;

    @TableField("icon")
    private String icon;

    @TableField("aqara_position_id")
    private String aqaraPositionId;

    @TableField("description")
    private String description;

    @TableField("create_by")
    private String createBy;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_by")
    private String updateBy;

    @TableField("update_time")
    private Date updateTime;

}