package com.yema.graphql.demo.service.impl;

import com.yema.graphql.demo.service.IFamilyRoomService;
import org.springframework.stereotype.Service;

/**
 * @Author: wanjianghongfeng@yematech.cn
 * @Description:  服务实现类
 * @Date: 2021-07-08
 **/
@Service
public class FamilyRoomServiceImpl implements IFamilyRoomService {
}

